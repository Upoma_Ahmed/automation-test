**DOWNLOAD & IMPORT PROJECT**

1.Launch IntelliJ IDEA.

2.If the Welcome screen opens, click Import Project.

Otherwise, from the main menu, select File | New | Project from Existing Sources.

3.In the dialog that opens, select the directory in which your sources, libraries, and other assets are located and click Open.

4.Select the project type:

    -Maven

    Create project from existing sources (if your project has another origin)
    
**IMPORT PROJECT FROM GITLAB**

 IntelliJ IDEA allows us to check out (in Git terms clone) an existing repository and create a new project based on the data we've downloaded.

    1.From the main menu, choose VCS | Checkout from Version Control | Git, or, if no project is currently opened, choose Checkout from Version Control | Git on the Welcome screen.

    2.In the Clone Repository dialog, specify the URL of the remote repository you want to clone (you can click Test to make sure that connection to the remote can be established).

    3.In the Directory field, specify the path where the folder for your local Git repository will be created into which the remote repository will be cloned.

    4.Click Clone. If you want to create a IntelliJ IDEA project based on the sources you have cloned, click Yes in the confirmation dialog. Git root mapping will be automatically set to the project root directory.

**REQUIREMENTS**
pom.xml - (must contain)
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.blazemeter.selenium-tutorial</groupId>
    <artifactId>JUnitWithSelenium</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
    <!-- https://mvnrepository.com/artifact/junit/junit -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>3.8.1</version>
        </dependency>
    </dependencies>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
                <version>2.20.1</version>
            </plugin>
        </plugins>
    </reporting>
</project>

-it will show a pop up suggesting to automatically download and install dependencies.