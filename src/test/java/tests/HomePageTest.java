package tests;

import Pages.HomePage;
import environment.EnvironmentManager;
import environment.RunEnvironment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Keys;
import java.util.Scanner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class HomePageTest {

    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }

    //Checking nav_bar options name/text
    @Test
    public void Nbar_elements_text() {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);
        hp.navigationbar_elements_text();
    }

    //Checking available options on top of nav_bar
    @Test
    public void Nbar_upper_Elements()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);
        hp.nav_bar_upper_elements_text();

    }

    //Checking URL and page title of nav_bar options pages
    @Test
    public void Nbar_elements_url () throws InterruptedException
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);
        hp.getHome_btn().click();
        String home_url = driver.getCurrentUrl();
        String home_title = driver.getTitle();
        assertEquals(home_url, "https://www.aiub.edu/");
        assertEquals(home_title, "Home | American International University-Bangladesh (AIUB)");

        hp.getAbout_drp_dwn().click();
        String about_url = driver.getCurrentUrl();
        String about_title = driver.getTitle();
        assertEquals(about_url, "https://www.aiub.edu/about");
        assertEquals(about_title, "About Us | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getAcademics_drp_dwn().click();
        String academics_url = driver.getCurrentUrl();
        String academics_title = driver.getTitle();
        assertEquals(academics_url, "https://www.aiub.edu/academics");
        assertEquals(academics_title, "Academics | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getAdminstration_drp_dwn().click();
        String admin_url = driver.getCurrentUrl();
        String admin_title = driver.getTitle();
        assertEquals(admin_url, "https://www.aiub.edu/administration");
        assertEquals(admin_title, "Administration | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getFaculties_drp_dwn().click();
        String faculties_url = driver.getCurrentUrl();
        String faculties_title = driver.getTitle();
        assertEquals(faculties_url, "https://www.aiub.edu/faculties");
        assertEquals(faculties_title, "Faculties | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getOffices_drp_dwn().click();
        String offices_url = driver.getCurrentUrl();
        String offices_title = driver.getTitle();
        assertEquals(offices_url, "https://www.aiub.edu/offices");
        assertEquals(offices_title, "Offices | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getAdmission_drp_dwn().click();
        String admission_url = driver.getCurrentUrl();
        String admission_title = driver.getTitle();
        assertEquals(admission_url, "https://www.aiub.edu/admission");
        assertEquals(admission_title, "Admission | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        hp.getContact_btn().click();
        String contact_url = driver.getCurrentUrl();
        String contact_title = driver.getTitle();
        assertEquals(contact_url, "https://www.aiub.edu/contact");
        assertEquals(contact_title, "Contact Us | American International University-Bangladesh (AIUB)");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

        /*hp.webmail_btn_click();
        String WebMail_url = driver.getCurrentUrl();
        assertEquals(WebMail_url, "https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&response_mode=form_post&response_type=code+id_token&scope=openid+profile&state=OpenIdConnect.AuthenticationProperties%3dt_B1sSC_jITRV9Hc6NTQBoXeP5bZAvJgrfMQLfcdkApEQNwkqopFfkoeaJUBoNrR5YaS-D13MWzYsVgA-vTO10K4X3a3fwh9CVw9p9JPbdTMo-HarFwUR9g9kfHRCim001L3-kWt6p_fXiMKClDByg&nonce=637078355137800940.MmM0ZGY1ZjUtZjZmZC00YWM1LTk4YzAtYTgzYmIyY2Q3NWE5OGEzNTg5ODMtMTJlMS00Y2U1LWJmYjEtZGIxMWJlOWJkNjY2&redirect_uri=https%3a%2f%2fwww.office.com%2f&ui_locales=en-US&mkt=en-US&client-request-id=5ffdee68-a24e-45c5-b9f1-1fe92d37f453");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();*/

        hp.getLogin_btn().click();
        String login_url = driver.getCurrentUrl();
        String login_title = driver.getTitle();
        assertEquals(login_url, "https://portal.aiub.edu/");
        assertEquals(login_title, "Index");
        TimeUnit.SECONDS.sleep(1);
        driver.navigate().back();

    }

    //Checking about_drop-downs items from nav_bar
    @Test
    public void about_dropDown_items_check() throws InterruptedException
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getAbout_drp_dwn()).perform();

        String about_item1 = hp.getAbout_item1().getText();
        assertEquals(about_item1, "Information");
        String about_item2 = hp.getAbout_item2().getText();
        assertEquals(about_item2, "General Information");
        String about_item3 = hp.getAbout_item3().getText();
        assertEquals(about_item3, "Rules of Campus Entry");
        String about_item4 = hp.getAbout_item4().getText();
        assertEquals(about_item4, "Why Study Here");
        String about_item5 = hp.getAbout_item5().getText();
        assertEquals(about_item5, "Resources");
        String about_item6 = hp.getAbout_item6().getText();
        assertEquals(about_item6, "Career");
        String about_item7 = hp.getAbout_item7().getText();
        assertEquals(about_item7, "Convocation");
        String about_item8 = hp.getAbout_item8().getText();
        assertEquals(about_item8, "Video");

        TimeUnit.SECONDS.sleep(1);
    }

    //Checking academics_drop-downs items from nav_bar
    @Test
    public void academics_dropDown_items_check()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getAcademics_drp_dwn()).perform();

        String academics_item1 = hp.getAcademics_item1().getText();
        assertEquals(academics_item1, "Academic Regulations");

        String academics_item2 = hp.getAcademics_item2().getText();
        assertEquals(academics_item2, "Courses & Tuition Fees");

        String academics_item3 = hp.getAcademics_item3().getText();
        assertEquals(academics_item3, "Academic Calendar");

    }

    //Checking administration_drop-downs items from nav_bar
    @Test
    public void adminstration_dropdown_items_check ()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getAdminstration_drp_dwn()).perform();

        String admin_item1 = hp.getAdmin_item1().getText();
        assertEquals(admin_item1, "The Vice Chancellor");

        String admin_item2 = hp.getAdmin_item2().getText();
        assertEquals(admin_item2, "The Chairman");

        String admin_item3 = hp.getAdmin_item3().getText();
        assertEquals(admin_item3, "The Founders");

    }

    //Checking faculties_drop-downs items from nav_bar
    @Test
    public void faculties_dropdown_items_check()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getFaculties_drp_dwn()).perform();

        String faculties_item1 = hp.getFaculties_item1().getText();
        assertEquals(faculties_item1, "Faculty of Arts And Social Science");

        String faculties_item2 = hp.getFaculties_item2().getText();
        assertEquals(faculties_item2, "Faculty of Business Administration");

        String faculties_item3 = hp.getFaculties_item3().getText();
        assertEquals(faculties_item3, "Faculty of Engineering");

        String faculties_item4 = hp.getFaculties_item4().getText();
        assertEquals(faculties_item4, "Faculty of Science And Technology");

        String faculties_item5 = hp.getFaculties_item5().getText();
        assertEquals(faculties_item5, "Faculty List");

    }

    //Checking faculties_drop-downs items from nav_bar
    @Test
    public void admission_dropdown_items_check()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getAdmission_drp_dwn()).perform();

        String admission_item1 = hp.getAdmission_item1().getText();
        assertEquals(admission_item1, "Admission Information");

        String admission_item2 = hp.getAdmission_item2().getText();
        assertEquals(admission_item2, "Online Admission");
    }

    @Test
    public void offices_dropdown_items_check()
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        Actions action = new Actions(driver);
        action.moveToElement(hp.getOffices_drp_dwn()).perform();

        String offices_item1 = hp.getOffice_item1().getText();
        assertEquals(offices_item1, "Office of Planning & Development (OPD)");

        String offices_item2 = hp.getOffice_item2().getText();
        assertEquals(offices_item2, "Office of Cultural Affairs (OCA)");

        String offices_item3 = hp.getOffice_item3().getText();
        assertEquals(offices_item3, "Office of Sports (OS)");

        String offices_item4 = hp.getOffice_item4().getText();
        assertEquals(offices_item4, "Office of Probation (OP)");

        String offices_item5 = hp.getOffice_item5().getText();
        assertEquals(offices_item5, "Office of Placement & Alumni (OPA)");

        String offices_item6 = hp.getOffice_item6().getText();
        assertEquals(offices_item6, "Office of Students Affairs (OSA)");

        String offices_item7 = hp.getOffice_item7().getText();
        assertEquals(offices_item7, "AIUB Center for Research and Excellence (ACRE)");

        String offices_item8 = hp.getOffice_item8().getText();
        assertEquals(offices_item8, "Office of Public Relations (OPR)");

        String offices_item9 = hp.getOffice_item9().getText();
        assertEquals(offices_item9, "Office of Finance and Audit (OFA)");

        String offices_item10 = hp.getOffice_item10().getText();
        assertEquals(offices_item10, "Office of the Controller of Examinations");

        String offices_item11 = hp.getOffice_item11().getText();
        assertEquals(offices_item11, "Office of the Registrar");
    }

    @Test
    public void searchBox_text() throws InterruptedException
    {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://www.aiub.edu");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        HomePage hp = PageFactory.initElements(driver, HomePage.class);
        String srch_in = "ACC";
        hp.getSearch_txtbox().sendKeys(srch_in);
        hp.getSearch_txtbox().sendKeys(Keys.ENTER);

        String search_url = driver.getCurrentUrl();
        TimeUnit.SECONDS.sleep(1);
        assertEquals(search_url, "https://www.aiub.edu/search/?key="+srch_in);
        //System.out.println(search_url);

    }

    @After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }
}
