package Pages;

import environment.EnvironmentManager;
import environment.RunEnvironment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class HomePage{

    //nav_bar_elements
    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[1]/a")
    WebElement home_btn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/a")
    WebElement about_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[3]/a")
    WebElement academics_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[4]/a")
    WebElement adminstration_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/a")
    WebElement faculties_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/a")
    WebElement offices_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[7]/a")
    WebElement admission_drp_dwn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[8]/a")
    WebElement contact_btn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[1]/li[1]/a")
    WebElement webMail_btn;

    @FindBy(how = How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[1]/li[2]/a")
    WebElement login_btn;

    //upper nav bar elements
    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[2]/a[1]")
    WebElement alumni_job_alert_btn;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[2]/a[2]")
    WebElement library_btn;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[2]/a[3]")
    WebElement convo_btn;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[2]/a[4]")
    WebElement research_btn;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[2]/a[5]")
    WebElement alumni_btn;

    //About drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[1]/a")
    WebElement about_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[2]/a")
    WebElement about_item2;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[3]/a")
    WebElement about_item3;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[4]/a")
    WebElement about_item4;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[5]/a")
    WebElement about_item5;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[6]/a")
    WebElement about_item6;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[7]/a")
    WebElement about_item7;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[2]/ul/li[8]/a")
    WebElement about_item8;

    //Academics drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[3]/ul/li[1]/a")
    WebElement academics_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[3]/ul/li[2]/a")
    WebElement academics_item2;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[3]/ul/li[3]/a")
    WebElement academics_item3;

    //Administration drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[4]/ul/li[1]/a")
    WebElement admin_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[4]/ul/li[2]/a")
    WebElement admin_item2;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[4]/ul/li[3]/a")
    WebElement admin_item3;

    //faculties drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/ul/li[1]/a")
    WebElement faculties_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/ul/li[2]/a")
    WebElement faculties_item2;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/ul/li[3]/a")
    WebElement faculties_item3;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/ul/li[4]/a")
    WebElement faculties_item4;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[5]/ul/li[5]/a")
    WebElement faculties_item5;

    //Offices drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[1]/a")
    WebElement office_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[2]/a")
    WebElement office_item2;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[3]/a")
    WebElement office_item3;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[4]/a")
    WebElement office_item4;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[5]/a")
    WebElement office_item5;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[6]/a")
    WebElement office_item6;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[7]/a")
    WebElement office_item7;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[8]/a")
    WebElement office_item8;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[9]/a")
    WebElement office_item9;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[10]/a")
    WebElement office_item10;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[6]/ul/li[11]/a")
    WebElement office_item11;

    //Admission drop down items
    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[7]/ul/li[1]/a")
    WebElement admission_item1;

    @FindBy(how=How.XPATH, using = "/html/body/div[3]/div/div[2]/ul[2]/li[7]/ul/li[2]/a")
    WebElement admission_item2;

    //Search box
    @FindBy(how=How.ID, using = "key")
    WebElement search_txtbox;

    @FindBy(how= How.XPATH, using = "/html/body/div[2]/div/div/div[2]/div[1]/form/div/span/button/span")
    WebElement search_icon;

    public void navigationbar_elements_text()
    {
        String home_txt = home_btn.getText();
        assertEquals(home_txt, "Home");

        String abt_txt = about_drp_dwn.getText();
        assertEquals(abt_txt, "About");

        String academics_txt = academics_drp_dwn.getText();
        assertEquals(academics_txt, "Academics");

        String admins_txt = adminstration_drp_dwn.getText();
        assertEquals(admins_txt, "Administration");

        String faculties_txt = faculties_drp_dwn.getText();
        assertEquals(faculties_txt, "Faculties");

        String office_txt = offices_drp_dwn.getText();
        assertEquals(office_txt, "Offices");

        String admission_txt = admission_drp_dwn.getText();
        assertEquals(admission_txt, "Admission");

        String ct = contact_btn.getText();
        assertEquals(ct, "Contact");

        String webmail_txt = webMail_btn.getText();
        assertEquals(webmail_txt, "Web Mail");

        String login_txt = login_btn.getText();
        assertEquals(login_txt, "Login");

    }

    public void nav_bar_upper_elements_text()
    {
        String alumni_job_alert_txt = alumni_job_alert_btn.getText();
        assertEquals(alumni_job_alert_txt, "Alumni Job Alert");

        String library_txt = library_btn.getText();
        assertEquals(library_txt, "Library");

        String convo_txt = convo_btn.getText();
        assertEquals(convo_txt, "Convocation");

        String research_txt = research_btn.getText();
        assertEquals(research_txt, "Research");

        String alumni_txt = alumni_btn.getText();
        assertEquals(alumni_txt, "Alumni");
    }



    public void setHome_btn(WebElement hb) { this.home_btn = hb; }
    public WebElement getHome_btn() { return home_btn; }

    public void setAbout_drp_dwn(WebElement abt_dd) { this.about_drp_dwn = abt_dd; }
    public WebElement getAbout_drp_dwn() { return about_drp_dwn; }

    public void setAcademics_drp_dwn(WebElement aca_dd) { this.about_drp_dwn = aca_dd; }
    public WebElement getAcademics_drp_dwn() { return academics_drp_dwn; }

    public void setAdminstration_drp_dwn(WebElement admin_dd) { this.adminstration_drp_dwn = admin_dd; }
    public WebElement getAdminstration_drp_dwn() { return adminstration_drp_dwn; }

    public void setFaculties_drp_dwn(WebElement faculties_dd) { this.faculties_drp_dwn = faculties_dd; }
    public WebElement getFaculties_drp_dwn() { return faculties_drp_dwn; }

    public void setOffices_drp_dwn(WebElement offices_dd) { this.offices_drp_dwn = offices_dd; }
    public WebElement getOffices_drp_dwn() { return offices_drp_dwn; }

    public void setAdmission_drp_dwn(WebElement admission_dd) { this.admission_drp_dwn = admission_dd; }
    public WebElement getAdmission_drp_dwn() { return admission_drp_dwn; }

    public void setContact_btn(WebElement con_btn) { this.contact_btn = con_btn; }
    public WebElement getContact_btn() { return contact_btn; }

    public void setWebMail_btn(WebElement webMail_btn) { this.webMail_btn = webMail_btn; }
    public WebElement getWebMail_btn() { return webMail_btn; }

    public void setLogin_btn(WebElement login_btn) { this.login_btn = login_btn; }
    public WebElement getLogin_btn() { return login_btn; }

    public void setAlumni_job_alert_btn(WebElement alumni_job_alert_btn) { this.alumni_job_alert_btn = alumni_job_alert_btn; }
    public WebElement getAlumni_job_alert_btn() { return alumni_job_alert_btn; }

    public void setLibrary_btn(WebElement library_btn) { this.library_btn = library_btn; }
    public WebElement getLibrary_btn() { return library_btn; }

    public void setConvo_btn(WebElement convo_btn) { this.convo_btn = convo_btn; }
    public WebElement getConvo_btn() { return convo_btn; }

    public void setResearch_btn(WebElement research_btn) { this.research_btn = research_btn; }
    public WebElement getResearch_btn() { return research_btn; }

    public void setAlumni_btn(WebElement alumni_btn) { this.alumni_btn = alumni_btn; }
    public WebElement getAlumni_btn() { return alumni_btn; }

    //About drop down items
    public void setAbout_item1(WebElement about_item1) { this.about_item1 = about_item1; }
    public WebElement getAbout_item1() { return about_item1; }

    public void setAbout_item2(WebElement about_item2) { this.about_item2 = about_item2; }
    public WebElement getAbout_item2() { return about_item2; }

    public void setAbout_item3(WebElement about_item3) { this.about_item3 = about_item3; }
    public WebElement getAbout_item3() { return about_item3; }

    public void setAbout_item4(WebElement about_item4) { this.about_item4 = about_item4; }
    public WebElement getAbout_item4() { return about_item4; }

    public void setAbout_item5(WebElement about_item5) { this.about_item5 = about_item5; }
    public WebElement getAbout_item5() { return about_item5; }

    public void setAbout_item6(WebElement about_item6) { this.about_item6 = about_item6; }
    public WebElement getAbout_item6() { return about_item6; }

    public void setAbout_item7(WebElement about_item7) { this.about_item7 = about_item7; }
    public WebElement getAbout_item7() { return about_item7; }

    public void setAbout_item8(WebElement about_item8) { this.about_item8 = about_item8; }
    public WebElement getAbout_item8() { return about_item8; }

    //Academics drop down items
    public void setAcademics_item1(WebElement academics_item1) { this.academics_item1 = academics_item1; }
    public WebElement getAcademics_item1() { return academics_item1; }

    public void setAcademics_item2(WebElement academics_item2) { this.academics_item2 = academics_item2; }
    public WebElement getAcademics_item2() { return academics_item2; }

    public void setAcademics_item3(WebElement academics_item3) { this.academics_item3 = academics_item3; }
    public WebElement getAcademics_item3() { return academics_item3; }

    //Administration drop down items
    public void setAdmin_item1(WebElement admin_item1) { this.admin_item1 = admin_item1; }
    public WebElement getAdmin_item1() { return admin_item1;}

    public void setAdmin_item2(WebElement admin_item2) { this.admin_item2 = admin_item2; }
    public WebElement getAdmin_item2() { return admin_item2; }

    public void setAdmin_item3(WebElement admin_item3) { this.admin_item3 = admin_item3; }
    public WebElement getAdmin_item3() { return admin_item3; }

    //Faculties drop down items
    public void setFaculties_item1(WebElement faculties_item1) { this.faculties_item1 = faculties_item1; }
    public WebElement getFaculties_item1() { return faculties_item1; }

    public void setFaculties_item2(WebElement faculties_item2) { this.faculties_item2 = faculties_item2; }
    public WebElement getFaculties_item2() { return faculties_item2; }

    public void setFaculties_item3(WebElement faculties_item3) { this.faculties_item3 = faculties_item3; }
    public WebElement getFaculties_item3() { return faculties_item3; }

    public void setFaculties_item4(WebElement faculties_item4) { this.faculties_item4 = faculties_item4; }
    public WebElement getFaculties_item4() { return faculties_item4; }

    public void setFaculties_item5(WebElement faculties_item5) { this.faculties_item5 = faculties_item5; }
    public WebElement getFaculties_item5() { return faculties_item5; }

    //Offices drop down items
    public void setOffice_item1(WebElement office_item1) { this.office_item1 = office_item1; }
    public WebElement getOffice_item1() { return office_item1; }

    public void setOffice_item2(WebElement office_item2) { this.office_item2 = office_item2; }
    public WebElement getOffice_item2() { return office_item2; }

    public void setOffice_item3(WebElement office_item3) { this.office_item3 = office_item3; }
    public WebElement getOffice_item3() { return office_item3; }

    public void setOffice_item4(WebElement office_item4) { this.office_item4 = office_item4; }
    public WebElement getOffice_item4() { return office_item4; }

    public void setOffice_item5(WebElement office_item5) { this.office_item5 = office_item5; }
    public WebElement getOffice_item5() { return office_item5; }

    public void setOffice_item6(WebElement office_item6) { this.office_item6 = office_item6; }
    public WebElement getOffice_item6() { return office_item6; }

    public void setOffice_item7(WebElement office_item7) { this.office_item7 = office_item7; }
    public WebElement getOffice_item7() { return office_item7; }

    public void setOffice_item8(WebElement office_item8) { this.office_item8 = office_item8; }
    public WebElement getOffice_item8() { return office_item8; }

    public void setOffice_item9(WebElement office_item9) { this.office_item9 = office_item9; }
    public WebElement getOffice_item9() { return office_item9; }

    public void setOffice_item10(WebElement office_item10) { this.office_item10 = office_item10; }
    public WebElement getOffice_item10() { return office_item10; }

    public void setOffice_item11(WebElement office_item11) { this.office_item11 = office_item11; }
    public WebElement getOffice_item11() { return office_item11; }

    //Admission drop down items
    public void setAdmission_item1(WebElement admission_item1) { this.admission_item1 = admission_item1; }
    public WebElement getAdmission_item1() { return admission_item1; }

    public void setAdmission_item2(WebElement admission_item2) { this.admission_item2 = admission_item2; }
    public WebElement getAdmission_item2() { return admission_item2; }

    //search box
    public void setSearch_txtbox() { this.search_txtbox = search_txtbox; }
    public WebElement getSearch_txtbox() { return search_txtbox; }

    public void setSearch_icon() { this.search_icon = search_icon; }
    public WebElement getSearch_icon() { return search_icon; }


}
